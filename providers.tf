terraform {
  required_version = "~> 1.3"

  backend "s3" {}

  required_providers {
    archive = {
      source  = "hashicorp/archive"
      version = "~> 2.2"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.49"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.2"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.4"
    }
    # time = {
    #   source  = "hashicorp/time"
    #   version = "~> 0.11"
    # }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = var.region
}

provider "aws" {
  alias  = "sms"
  region = var.sms_region
}

provider "aws" {
  alias  = "cloudfront"
  region = var.cloudfront_region
}

provider "archive" {}

provider "local" {}

provider "random" {}

provider "tls" {}
