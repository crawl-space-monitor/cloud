locals {
  owner_domain  = "paullalonde.ca"
  app_name      = "crawl-space-monitor"
  app_site_name = "${local.app_name}-site"
}

resource "aws_s3_bucket" "app_site" {
  bucket_prefix = "${local.app_site_name}-"
  force_destroy = false
}

resource "aws_s3_bucket_policy" "app_site" {
  bucket = aws_s3_bucket.app_site.id
  policy = data.aws_iam_policy_document.app_site_bucket_policy.json
}

data "aws_iam_policy_document" "app_site_bucket_policy" {
  statement {
    sid       = "Deny Unencrypted Object Uploads"
    effect    = "Deny"
    actions   = ["s3:PutObject"]
    resources = ["${aws_s3_bucket.app_site.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption"
      values   = ["AES256"]
    }
  }

  statement {
    sid     = "Deny Insecure Traffic"
    effect  = "Deny"
    actions = ["s3:*"]

    resources = [
      aws_s3_bucket.app_site.arn,
      "${aws_s3_bucket.app_site.arn}/*",
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = ["false"]
    }
  }

  statement {
    sid       = "CloudFront Access"
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.app_site.arn}/*"]

    principals {
      type        = "Service"
      identifiers = ["cloudfront.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "aws:SourceArn"
      values = [
        aws_cloudfront_distribution.app_site.arn,
      ]
    }
  }
}

resource "aws_s3_bucket_ownership_controls" "app_site" {
  bucket = aws_s3_bucket.app_site.id

  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "app_site" {
  bucket = aws_s3_bucket.app_site.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "app_site" {
  bucket                  = aws_s3_bucket.app_site.bucket
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_versioning" "app_site" {
  bucket = aws_s3_bucket.app_site.id

  versioning_configuration {
    status = "Enabled"
  }
}

# resource "aws_s3_bucket_logging" "app_site" {
#   bucket = aws_s3_bucket.public_assets.id

#   target_bucket = aws_s3_bucket.s3_access_logs.id
#   target_prefix = "Logs/S3/${aws_s3_bucket.app_site.bucket}/"

#   target_object_key_format {
#     partitioned_prefix {
#       partition_date_source = "DeliveryTime"
#     }
#   }
# }

resource "aws_s3_object" "app_site_index" {
  key                    = "index.html"
  content                = file("${path.module}/documents/app-site/index.html")
  content_type           = "text/html"
  bucket                 = aws_s3_bucket.app_site.id
  server_side_encryption = "AES256"
}

resource "aws_s3_object" "app_site_apple_app_site_association" {
  key                    = ".well-known/apple-app-site-association"
  content                = file("${path.module}/documents/app-site/apple-app-site-association.json")
  content_type           = "application/json"
  bucket                 = aws_s3_bucket.app_site.id
  server_side_encryption = "AES256"
}

locals {
  cloudfront_app_site_origin_id = "${local.app_site_name}-origin-id"
  cloudfront_app_site_fqdn      = "${local.app_name}.${local.owner_domain}"
}

data "aws_route53_zone" "owner" {
  name = "${local.owner_domain}."
}

resource "aws_route53_record" "app_site" {
  zone_id = data.aws_route53_zone.owner.zone_id
  name    = local.app_name
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.app_site.domain_name
    zone_id                = aws_cloudfront_distribution.app_site.hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_acm_certificate" "app_site" {
  provider          = aws.cloudfront
  domain_name       = local.cloudfront_app_site_fqdn
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

locals {
  app_site_certificate_validation = one(aws_acm_certificate.app_site.domain_validation_options)
}

resource "aws_acm_certificate_validation" "app_site" {
  provider                = aws.cloudfront
  certificate_arn         = aws_acm_certificate.app_site.arn
  validation_record_fqdns = [local.app_site_certificate_validation.resource_record_name]
}

# Enregistrements DNS pour valider chaque nom de domaine géré par Route53.
resource "aws_route53_record" "app_site_certificate_validation" {
  allow_overwrite = true
  name            = local.app_site_certificate_validation.resource_record_name
  records         = [local.app_site_certificate_validation.resource_record_value]
  ttl             = 60
  type            = local.app_site_certificate_validation.resource_record_type
  zone_id         = data.aws_route53_zone.owner.zone_id
}

resource "time_sleep" "wait_after_app_site_certificate_creation" {
  depends_on = [
    aws_acm_certificate.app_site,
    aws_acm_certificate_validation.app_site,
    aws_route53_record.app_site_certificate_validation,
  ]

  create_duration = "60s"
}

resource "aws_cloudfront_distribution" "app_site" {
  origin {
    connection_attempts      = 3
    connection_timeout       = 10
    domain_name              = aws_s3_bucket.app_site.bucket_regional_domain_name
    origin_access_control_id = aws_cloudfront_origin_access_control.app_site.id
    origin_id                = local.cloudfront_app_site_origin_id
  }

  aliases             = [local.cloudfront_app_site_fqdn]
  enabled             = true
  comment             = "Crawl Space Monitor Site; géré par Terraform."
  default_root_object = "index.html"
  http_version        = "http2"
  is_ipv6_enabled     = true

  default_cache_behavior {
    cache_policy_id        = aws_cloudfront_cache_policy.app_site.id
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    compress               = true
    target_origin_id       = local.cloudfront_app_site_origin_id
    viewer_protocol_policy = "redirect-to-https"
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.app_site.arn
    minimum_protocol_version = "TLSv1.2_2021"
    ssl_support_method       = "sni-only"
  }

  # logging_config {
  #   include_cookies = false
  #   bucket          = aws_s3_bucket.s3_access_logs.bucket_domain_name
  #   prefix          = "Logs/CloudFront/${local.public_assets_name}/"
  # }

  tags = {
    Name = local.app_site_name
  }

  depends_on = [
    time_sleep.wait_after_app_site_certificate_creation,
  ]
}

resource "aws_cloudfront_origin_access_control" "app_site" {
  name                              = local.app_site_name
  description                       = "Géré par Terraform."
  origin_access_control_origin_type = "s3"
  signing_behavior                  = "always"
  signing_protocol                  = "sigv4"
}

resource "aws_cloudfront_cache_policy" "app_site" {
  name        = local.app_site_name
  comment     = "Géré par Terraform."
  default_ttl = 0
  max_ttl     = 0
  min_ttl     = 0

  parameters_in_cache_key_and_forwarded_to_origin {
    enable_accept_encoding_brotli = false
    enable_accept_encoding_gzip   = false

    cookies_config {
      cookie_behavior = "none"
    }

    headers_config {
      header_behavior = "none"
    }

    query_strings_config {
      query_string_behavior = "none"
    }
  }
}
