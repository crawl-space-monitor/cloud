# -------------------------------------------------------------------
# Cognito User Pool & related resources
# -------------------------------------------------------------------

resource "aws_cognito_user_pool" "user_pool" {
  name = "${var.group_name}-auth"

  admin_create_user_config {
    allow_admin_create_user_only = true
  }

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    name                     = "email"
    mutable                  = true
    required                 = true

    string_attribute_constraints {
      max_length = 2048
      min_length = 0
    }
  }

  auto_verified_attributes  = ["email"]

  password_policy {
    minimum_length    = 8
    require_lowercase = true
    require_numbers   = true
    require_symbols   = false
    require_uppercase = true

    temporary_password_validity_days = 7
  }

  tags = {
    application = var.group_name
  }
}

resource "aws_cognito_user_pool_domain" "user_pool" {
  domain       = "crawl-space-monitor"
  user_pool_id = aws_cognito_user_pool.user_pool.id
}

resource "aws_cognito_user_pool_client" "ios_app" {
  name                   = "ios-app"
  user_pool_id           = aws_cognito_user_pool.user_pool.id
  refresh_token_validity = 365

  explicit_auth_flows = [
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_USER_SRP_AUTH",
  ]
}

resource "aws_cognito_identity_pool" "id_pool" {
  identity_pool_name               = "Crawl Space Monitor Identites"
  allow_unauthenticated_identities = true

  cognito_identity_providers {
    client_id     = aws_cognito_user_pool_client.ios_app.id
    provider_name = aws_cognito_user_pool.user_pool.endpoint
  }
}

resource "aws_cognito_identity_pool_roles_attachment" "id_pool" {
  identity_pool_id = aws_cognito_identity_pool.id_pool.id

  roles = {
    "authenticated"   = aws_iam_role.id_pool_authenticated.arn
    "unauthenticated" = aws_iam_role.id_pool_unauthenticated.arn
  }
}

resource "aws_iam_role" "id_pool_authenticated" {
  name               = "${var.group_name}-id-pool-authenticated"
  assume_role_policy = data.aws_iam_policy_document.id_pool_authenticated_trust.json

  tags = {
    application = var.group_name
  }
}

data "aws_iam_policy_document" "id_pool_authenticated_trust" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = ["cognito-identity.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "cognito-identity.amazonaws.com:aud"
      values   = [ aws_cognito_identity_pool.id_pool.id ]
    }

    condition {
      test     = "ForAnyValue:StringLike"
      variable = "cognito-identity.amazonaws.com:amr"
      values   = [ "authenticated" ]
    }
  }
}

resource "aws_iam_role_policy" "id_pool_authenticated_metrics_policy" {
  name   = "get-metrics"
  role   = aws_iam_role.id_pool_authenticated.name
  policy = data.aws_iam_policy_document.get_metrics_policy.json
}

data "aws_iam_policy_document" "get_metrics_policy" {
  statement {
    sid = "Metrics"
    actions = [
      "cloudwatch:GetMetricData",
      "cloudwatch:GetMetricWidgetImage",
    ]
    resources = ["*"]
  }

  statement {
    sid = "Alarms"
    actions = [
      "cloudwatch:DescribeAlarms",
    ]
    resources = [ aws_cloudwatch_metric_alarm.temperature_low.arn ]
  }
}

resource "aws_iam_role_policy" "id_pool_authenticated_registrar_policy" {
  name   = "invoke-registrar"
  role   = aws_iam_role.id_pool_authenticated.name
  policy = data.aws_iam_policy_document.invoke_registrar_policy.json
}

data "aws_iam_policy_document" "invoke_registrar_policy" {
  statement {
    sid = "InvokeRegistrar"
    actions = [
      "lambda:InvokeFunction",
    ]
    resources = [
      aws_lambda_alias.registrar.arn,
      "${aws_lambda_function.registrar.arn}:*",
      aws_lambda_function.registrar.arn,
    ]
  }
}

resource "aws_iam_role" "id_pool_unauthenticated" {
  name               = "${var.group_name}-id-pool-unauthenticated"
  assume_role_policy = data.aws_iam_policy_document.id_pool_unauthenticated_trust.json

  tags = {
    application = var.group_name
  }
}

data "aws_iam_policy_document" "id_pool_unauthenticated_trust" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = ["cognito-identity.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "cognito-identity.amazonaws.com:aud"
      values   = [ aws_cognito_identity_pool.id_pool.id ]
    }

    condition {
      test     = "ForAnyValue:StringLike"
      variable = "cognito-identity.amazonaws.com:amr"
      values   = [ "unauthenticated" ]
    }
  }
}

resource "aws_iam_role_policy" "id_pool_unauthenticated_registrar_policy" {
  name   = "invoke-registrar"
  role   = aws_iam_role.id_pool_unauthenticated.name
  policy = data.aws_iam_policy_document.invoke_registrar_policy.json
}
