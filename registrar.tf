resource "aws_dynamodb_table" "devices" {
  name         = "${var.group_name}-devices"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "UserId"
  range_key    = "DeviceToken"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "DeviceToken"
    type = "S"
  }

  ttl {
    attribute_name = "TTL"
    enabled        = true
  }

  global_secondary_index {
    name               = "Devices"
    hash_key           = "DeviceToken"
    projection_type    = "INCLUDE"
    non_key_attributes = ["TTL"]
  }

  tags = {
    application = var.group_name
  }
}

locals {
  registrar_name          = "${var.group_name}-registrar"
  registrar_lambda_source = "${path.module}/documents/registrar.js"
  registrar_lambda_zip    = "${path.module}/documents/registrar-lambda.zip"
}

resource "aws_cloudwatch_log_group" "registrar_lambda" {
  name              = "/aws/lambda/${local.registrar_name}"
  retention_in_days = 7

  tags = {
    application = var.group_name
  }
}

resource "aws_iam_role" "registrar_lambda" {
  name               = local.registrar_name
  description        = "Lambda service role; managed by Terraform."
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy.json

  tags = {
    "Name"      = local.registrar_name
    application = var.group_name
  }
}

resource "aws_iam_role_policy" "registrar_lambda_policy" {
  name   = "lambda-policy"
  role   = aws_iam_role.registrar_lambda.id
  policy = data.aws_iam_policy_document.registrar_lambda_policy.json
}

data "aws_iam_policy_document" "registrar_lambda_policy" {
  statement {
    sid = "WriteLogs"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      # "${aws_cloudwatch_log_group.registrar_lambda.arn}:*",
      "${aws_cloudwatch_log_group.registrar_lambda.arn}:*:*",
    ]
  }

  statement {
    sid = "Database"
    actions = [
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:UpdateItem",
    ]
    resources = [aws_dynamodb_table.devices.arn]
  }

  # statement {
  #   sid = "Endpoints"
  #   actions = [
  #     "sns:CreatePlatformEndpoint",
  #     "sns:DeleteEndpoint",
  #     "sns:SetEndpointAttributes",
  #   ]
  #   resources = ["*"]
  # }

  # statement {
  #   sid = "Subscriptions"
  #   actions = [
  #     "sns:Subscribe",
  #     "sns:Unsubscribe",
  #   ]
  #   resources = [aws_sns_topic.bridged_notifications.arn]
  # }
}

data "archive_file" "registrar_lambda" {
  type        = "zip"
  source_file = local.registrar_lambda_source
  output_path = local.registrar_lambda_zip
}

resource "aws_lambda_function" "registrar" {
  function_name    = "${var.group_name}-registrar"
  description      = "Registers an iOS device to receive push notifications; managed by Terraform."
  role             = aws_iam_role.registrar_lambda.arn
  runtime          = "nodejs18.x"
  handler          = "registrar.handler"
  timeout          = 60
  filename         = local.registrar_lambda_zip
  source_code_hash = data.archive_file.bridge_lambda.output_base64sha256
  publish          = true

  environment {
    variables = {
      DYNAMODB_TABLE = aws_dynamodb_table.devices.name

      # APPLICATION_ARN_DEV  = aws_sns_platform_application.crawl_space_monitor_app_dev.arn
      # APPLICATION_ARN_PROD = aws_sns_platform_application.crawl_space_monitor_app_prod.arn
      # DEST_REGION          = var.sms_region
      # SNS_TOPIC_ARN        = aws_sns_topic.bridged_notifications.arn
    }
  }

  tags = {
    application = var.group_name
  }

  depends_on = [
    aws_iam_role_policy.registrar_lambda_policy,
    aws_cloudwatch_log_group.registrar_lambda,
  ]
}

resource "aws_lambda_alias" "registrar" {
  name             = "current"
  description      = "Currently published version; managed by Terraform"
  function_name    = aws_lambda_function.registrar.arn
  function_version = aws_lambda_function.registrar.version
}
