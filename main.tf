data "aws_caller_identity" "current" {}
data "aws_partition" "current" {}
data "aws_region" "current" {}

locals {
  aws_account_id = data.aws_caller_identity.current.account_id
  partition      = data.aws_partition.current.partition
  region         = data.aws_region.current.name
}

data "aws_iot_endpoint" "device" {
  endpoint_type = "iot:Data-ATS"
}

data "aws_iam_policy_document" "iot_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["iot.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "lambda_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

locals {
  cloudwatch_temperature_metric = "temperature"
}

resource "aws_cloudwatch_dashboard" "crawl-space" {
  dashboard_name = "maison"
  dashboard_body = templatefile("${path.module}/documents/dashboard.json", {
                      region             = var.region
                      namespace          = var.group_name
                      temperature_metric = local.cloudwatch_temperature_metric
                    })
}

resource "aws_sns_topic" "notifications" {
  name = "${var.group_name}-notifications"

  tags = {
    application = var.group_name
  }
}

resource "aws_cloudwatch_metric_alarm" "temperature_low" {
  alarm_name                = "${var.group_name}-temperature-too-low"
  alarm_description         = "La température du vide sanitaire est trop basse."
  namespace                 = var.group_name
  metric_name               = local.cloudwatch_temperature_metric
  comparison_operator       = "LessThanOrEqualToThreshold"
  threshold                 = var.temperature_alarm_threshold
  period                    = 1800
  evaluation_periods        = 1
  statistic                 = "Average"
  datapoints_to_alarm       = 1

  alarm_actions = [
    aws_sns_topic.notifications.arn,
  ]

  ok_actions = [
    aws_sns_topic.notifications.arn,
  ]

  insufficient_data_actions = [
    aws_sns_topic.notifications.arn,
  ]

  tags = {
    application = var.group_name
  }
}
