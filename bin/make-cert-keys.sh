#!/bin/bash

set -eux

SELF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
KEYS_DIR="${SELF_DIR}/../keys"
mkdir -p "${KEYS_DIR}"

openssl genrsa -out keys/sensor-private-key.pem 4096
chmod 600 keys/sensor-private-key.pem
openssl rsa -in keys/sensor-private-key.pem -outform PEM -pubout -out keys/sensor-public-key.pem
