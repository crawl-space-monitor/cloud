#!/usr/bin/env bash
#
# Assumes the CI Server role for the given AWS Account.
# This script is meant to be run on the CI Server.

set -eu

AWS_ACCOUNT=`aws sts get-caller-identity --query 'Account' --output text`
ROLE_ARN="arn:aws:iam::${AWS_ACCOUNT}:role/ci-server"
ROLE_SESSION="ci-server-${CI_PIPELINE_ID:-0}"

aws sts assume-role --role-arn "${ROLE_ARN}" --role-session-name "${ROLE_SESSION}" >role.json
export AWS_ACCESS_KEY_ID=`jq <role.json -r '.Credentials.AccessKeyId'`
export AWS_SECRET_ACCESS_KEY=`jq <role.json -r '.Credentials.SecretAccessKey'`
export AWS_SESSION_TOKEN=`jq <role.json -r '.Credentials.SessionToken'`
