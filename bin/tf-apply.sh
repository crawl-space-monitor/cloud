#!/bin/bash

set -euo pipefail

SELF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_DIR="${SELF_DIR}/.."

if [[ -f "${BASE_DIR}/.env" ]]; then
  source "${BASE_DIR}/.env"
fi

TERRAFORM="${TERRAFORM:-terraform}"

DOCUMENTS_DIR="${BASE_DIR}/documents"
KEYS_DIR="${SELF_DIR}/../keys"
mkdir -p "${KEYS_DIR}"

VARIABLES_FILE="${KEYS_DIR}/variables.tfvars"
rm -f "${VARIABLES_FILE}"
touch "${VARIABLES_FILE}"

pushd "${BASE_DIR}" >/dev/null

# The APNS certificate and private key are stored in encrypted form in the Git repository.
# The encryption key is stored in the macOS Keychain on my development machine.

for ENV in dev prod
do
  APNS_PASSWORD_KEYCHAIN_ITEM_NAME=crawls-space-monitor-apns-cert-password-${ENV}
  APNS_PASSWORD_KEYCHAIN_ITEM_ACCOUNT=${ENV}
  APNS_PASSWORD_PATH="${KEYS_DIR}/apns-password-${ENV}.txt"

  rm -f "${APNS_PASSWORD_PATH}"
  touch "${APNS_PASSWORD_PATH}"
  chmod 600 "${APNS_PASSWORD_PATH}"
  security find-generic-password -l "${APNS_PASSWORD_KEYCHAIN_ITEM_NAME}" -a "${APNS_PASSWORD_KEYCHAIN_ITEM_ACCOUNT}" -g 2>&1 >/dev/null \
    | sed 's/^password: "\([^"]*\)"/\1/' \
      >"${APNS_PASSWORD_PATH}"

  APNS_CREDENTIALS_PATH="${DOCUMENTS_DIR}/apns-credentials-${ENV}.p12"
  APNS_CERTIFICATE_PATH="${KEYS_DIR}/apns-certificate-${ENV}.pem"
  APNS_PRIVATE_KEY_PATH="${KEYS_DIR}/apns-private-key-${ENV}.pem"
  openssl pkcs12 -legacy -in "${APNS_CREDENTIALS_PATH}" -passin "file:${APNS_PASSWORD_PATH}" -nokeys \
    | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' >"${APNS_CERTIFICATE_PATH}"
  openssl pkcs12 -legacy -in "${APNS_CREDENTIALS_PATH}" -passin "file:${APNS_PASSWORD_PATH}" -nocerts -nodes \
    | sed -ne '/-BEGIN PRIVATE KEY-/,/-END PRIVATE KEY-/p' >"${APNS_PRIVATE_KEY_PATH}"

  echo "apns_${ENV}_certificate_path = \"${APNS_CERTIFICATE_PATH}\"" >>"${VARIABLES_FILE}"
  echo "apns_${ENV}_private_key_path = \"${APNS_PRIVATE_KEY_PATH}\"" >>"${VARIABLES_FILE}"
done

if [ -z "${SENSOR_PRIVATE_KEY_PATH:-}" ]; then
  SENSOR_PRIVATE_KEY_PATH="${KEYS_DIR}/sensor-private-key.pem"
fi

if [ -z "${SENSOR_CERTIFICATE_PATH:-}" ]; then
  SENSOR_CERTIFICATE_PATH="${KEYS_DIR}/sensor-certificate.pem"
fi

if [ -z "${APNS_TOKEN_PRIVATE_KEY_PATH:-}" ]; then
  APNS_TOKEN_PRIVATE_KEY_PATH="${KEYS_DIR}/apns_token_private_key.p8"
fi

{
  echo "sensor_certificate_path = \"${SENSOR_CERTIFICATE_PATH}\""
  echo "sensor_private_key_path = \"${SENSOR_PRIVATE_KEY_PATH}\""
  echo "apns_token_private_key_path = \"${APNS_TOKEN_PRIVATE_KEY_PATH}\""
} >>"${VARIABLES_FILE}"

${TERRAFORM} apply \
  -input=false \
  "-var-file=${VARIABLES_FILE}"

VARIABLES_YAML="${KEYS_DIR}/variables.yaml"
${TERRAFORM} output -json | \
  jq '{terraform_outputs: .}' | \
  yq e -P - >"${VARIABLES_YAML}"

popd >/dev/null
