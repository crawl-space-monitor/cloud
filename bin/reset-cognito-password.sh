#!/bin/bash

set -euo pipefail

SELF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_DIR="${SELF_DIR}/.."

function usage () {
  echo "usage: reset-cognito-password.sh [--user <username>] --password <password>" 2>&1
  exit 20
}

USER=paul
PASSWORD=''

while [[ $# -gt 0 ]]; do
  case $1 in
    --user)
      USER="$2"
      shift
      shift
      ;;

    --password)
      PASSWORD="$2"
      shift
      shift
      ;;

    *)
      usage
      ;;
  esac
done

if [[ -z "${USER}" ]]; then
  echo "reset-cognito-password.sh: user cannot be empty." 2>&1
  exit 10
fi

if [[ -z "${PASSWORD}" ]]; then
  usage
fi

if [[ -f "${BASE_DIR}/.env" ]]; then
  source "${BASE_DIR}/.env"
fi

KEYS_DIR="${SELF_DIR}/../keys"
VARIABLES_YAML="${KEYS_DIR}/variables.yaml"

REGION=$(yq '.terraform_outputs.aws_region.value' "${VARIABLES_YAML}")
USER_POOL_ID=$(yq '.terraform_outputs.cognito_user_pool_id.value' "${VARIABLES_YAML}")

aws cognito-idp admin-set-user-password --region "${REGION}" \
  --user-pool-id "${USER_POOL_ID}" \
  --username "${USER}" \
  --password "${PASSWORD}" \
  --permanent
