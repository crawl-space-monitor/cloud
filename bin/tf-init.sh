#!/bin/bash

set -euo pipefail

SELF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_DIR="${SELF_DIR}/.."

if [[ -f "${BASE_DIR}/.env" ]]; then
  source "${BASE_DIR}/.env"
fi

TERRAFORM="${TERRAFORM:-terraform}"

pushd "${BASE_DIR}" >/dev/null

# This must be distinct for each Terraform configuration.
STATE_KEY=crawl-space-monitor-cloud

CFN_STACK_NAME=terraform-bootstrap
CFN_STACK_REGION=ca-central-1
STATE_BUCKET=`aws cloudformation describe-stacks \
  --region "${CFN_STACK_REGION}" \
  --stack-name "${CFN_STACK_NAME}" \
  --query 'Stacks[0].Outputs[?OutputKey==\`RemoteStateBucket\`].[OutputValue][0][0]' \
  --output text`
LOCK_TABLE=`aws cloudformation describe-stacks \
  --region "${CFN_STACK_REGION}" \
  --stack-name "${CFN_STACK_NAME}" \
  --query 'Stacks[0].Outputs[?OutputKey==\`RemoteStateLockTable\`].[OutputValue][0][0]' \
  --output text`

${TERRAFORM} init -input=false \
  -backend-config="region=${CFN_STACK_REGION}" \
  -backend-config="bucket=${STATE_BUCKET}" \
  -backend-config="key=${STATE_KEY}" \
  -backend-config="dynamodb_table=${LOCK_TABLE}"

popd >/dev/null
