# Bridge Lambda

locals {
  bridge_lambda_name   = "${var.group_name}-bridge-lambda"
  bridge_lambda_source = "${path.module}/documents/bridge.js"
  bridge_lambda_zip    = "${path.module}/documents/bridge-lambda.zip"
}

resource "aws_cloudwatch_log_group" "bridge_lambda" {
  name              = "/aws/lambda/${local.bridge_lambda_name}"
  retention_in_days = 365

  tags = {
    application = var.group_name
  }
}

resource "aws_iam_role" "bridge_lambda" {
  name               = local.bridge_lambda_name
  description        = "Lambda service role; managed by Terraform."
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy.json

  tags = {
    "Name"      = local.bridge_lambda_name
    application = var.group_name
  }
}

resource "aws_iam_role_policy" "bridge_lambda_policy" {
  name   = "lambda-policy"
  role   = aws_iam_role.bridge_lambda.id
  policy = data.aws_iam_policy_document.bridge_lambda_policy.json
}

data "aws_iam_policy_document" "bridge_lambda_policy" {
  statement {
    sid = "WriteLogs"
    actions = [
      # "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      # "${aws_cloudwatch_log_group.bridge_lambda.arn}:*",
      "${aws_cloudwatch_log_group.bridge_lambda.arn}:*:*",
    ]
  }

  # statement {
  #   sid = "PublishNotifications"
  #   actions = [
  #     "sns:Publish",
  #   ]
  #   resources = [
  #     aws_sns_topic.bridged_notifications.arn,
  #   ]
  # }

  statement {
    sid = "Database"
    actions = [
      "dynamodb:Scan",
    ]
    resources = [aws_dynamodb_table.devices.arn]
  }

  statement {
    sid = "Notifications"
    actions = [
      "mobiletargeting:SendMessages",
    ]
    resources = ["${aws_pinpoint_app.crawl_space_monitor.arn}/messages"]
  }
}

data "archive_file" "bridge_lambda" {
  type        = "zip"
  source_file = local.bridge_lambda_source
  output_path = local.bridge_lambda_zip
}

resource "aws_lambda_function" "bridge_lambda" {
  function_name    = local.bridge_lambda_name
  description      = "Sends an SMS when a sensor alert is triggered; managed by Terraform."
  role             = aws_iam_role.bridge_lambda.arn
  runtime          = "nodejs18.x"
  handler          = "bridge.handler"
  timeout          = 30
  filename         = local.bridge_lambda_zip
  source_code_hash = data.archive_file.bridge_lambda.output_base64sha256

  environment {
    variables = {
      APPLICATION_ID = aws_pinpoint_app.crawl_space_monitor.application_id
      DEST_REGION    = var.sms_region
      DEST_TOPIC     = aws_sns_topic.bridged_notifications.arn
      DYNAMODB_TABLE = aws_dynamodb_table.devices.name
    }
  }

  tags = {
    "Name"      = local.bridge_lambda_name
    application = var.group_name
  }

  depends_on = [
    aws_iam_role_policy.bridge_lambda_policy,
    aws_cloudwatch_log_group.bridge_lambda,
  ]
}

resource "aws_lambda_permission" "bridge_lambda" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.bridge_lambda.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.notifications.arn
}

resource "aws_sns_topic_subscription" "bridge_lambda" {
  topic_arn = aws_sns_topic.notifications.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.bridge_lambda.arn
}

resource "aws_sns_topic" "bridged_notifications" {
  provider = aws.sms
  name     = "${var.group_name}-bridged-notifications"

  tags = {
    application = var.group_name
  }
}

resource "aws_sns_platform_application" "crawl_space_monitor_app_dev" {
  provider            = aws.sms
  name                = "crawl-space-monitor-app-dev"
  platform            = "APNS_SANDBOX"
  platform_credential = file(var.apns_dev_private_key_path)
  platform_principal  = file(var.apns_dev_certificate_path)

  success_feedback_sample_rate = 100
}

resource "aws_sns_platform_application" "crawl_space_monitor_app_prod" {
  provider            = aws.sms
  name                = "crawl-space-monitor-app-prod"
  platform            = "APNS"
  platform_credential = file(var.apns_prod_private_key_path)
  platform_principal  = file(var.apns_prod_certificate_path)

  success_feedback_sample_rate = 100
}

resource "aws_pinpoint_app" "crawl_space_monitor" {
  name = "crawl-space-monitor-app"
}

resource "aws_pinpoint_apns_channel" "crawl_space_monitor" {
  application_id                = aws_pinpoint_app.crawl_space_monitor.application_id
  default_authentication_method = "APNS_TOKEN"

  bundle_id    = var.apns_bundle_id
  team_id      = var.apns_team_id
  token_key    = file(var.apns_token_private_key_path)
  token_key_id = var.apns_token_key_id
}

resource "aws_pinpoint_apns_sandbox_channel" "crawl_space_monitor" {
  application_id                = aws_pinpoint_app.crawl_space_monitor.application_id
  default_authentication_method = "APNS_TOKEN"

  bundle_id    = var.apns_bundle_id
  team_id      = var.apns_team_id
  token_key    = file(var.apns_token_private_key_path)
  token_key_id = var.apns_token_key_id
}
