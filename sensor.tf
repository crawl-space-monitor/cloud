# Temperature Sensor resources

resource "random_uuid" "sensor_uuid" {}

locals {
  temperature_sensor_name = "temperature-sensor"
  global_sensor_name      = "${var.group_name}-${local.temperature_sensor_name}"
}

resource "tls_cert_request" "sensor" {
  private_key_pem = file(var.sensor_private_key_path)

  subject {
    common_name  = local.global_sensor_name
    organization = "Paul Lalonde enrg."
    locality     = "Montréal"
    province     = "Québec"
    country      = "CA"
  }
}

resource "aws_iot_certificate" "sensor" {
  csr    = tls_cert_request.sensor.cert_request_pem
  active = true
}

# Save the certificate locally, so we can transfer it to the device.
# Not really sensitive, but the diffs are annoying.
resource "local_sensitive_file" "sensor_certificate" {
  content         = aws_iot_certificate.sensor.certificate_pem
  filename        = var.sensor_certificate_path
  file_permission = "0644"
}

resource "aws_iot_thing_type" "sensor" {
  name = local.global_sensor_name

  properties {
    description = "The crawl space monitor's temperature sensor."
  }
}

resource "aws_iot_thing" "sensor" {
  name            = local.global_sensor_name
  thing_type_name = aws_iot_thing_type.sensor.name
  attributes      = {}
}

resource "aws_iot_thing_principal_attachment" "sensor" {
  principal = aws_iot_certificate.sensor.arn
  thing     = aws_iot_thing.sensor.name
}

resource "aws_iot_policy" "sensor" {
  name   = local.global_sensor_name
  policy = data.aws_iam_policy_document.sensor_iot_policy.json
}

resource "aws_iot_policy_attachment" "sensor" {
  policy = aws_iot_policy.sensor.name
  target = aws_iot_certificate.sensor.arn
}

data "aws_iam_policy_document" "sensor_iot_policy" {
  statement {
    sid     = "Connect"
    actions = ["iot:Connect"]
    resources = [
      "arn:aws:iot:${var.region}:${local.aws_account_id}:client/$${iot:Connection.Thing.ThingName}",
    ]
    condition {
      test     = "Bool"
      variable = "iot:Connection.Thing.IsAttached"
      values   = ["true"]
    }
  }

  # Things can publish and subscribe to topics under their own name.
  statement {
    sid = "Publish"
    actions = [
      "iot:Publish",
    ]
    resources = [
      "arn:aws:iot:${var.region}:${local.aws_account_id}:topic/$aws/things/$${iot:Connection.Thing.ThingName}/*",
    ]
  }

  statement {
    sid = "Subscribe"
    actions = [
      "iot:Subscribe",
    ]
    resources = [
      "arn:aws:iot:${var.region}:${local.aws_account_id}:topicfilter/$aws/things/$${iot:Connection.Thing.ThingName}/*",
    ]
  }

  # Things can receive messages on all topics under their own name.
  statement {
    sid     = "Receive"
    actions = ["iot:Receive"]
    resources = [
      "arn:aws:iot:${var.region}:${local.aws_account_id}:topicfilter/$aws/things/$${iot:Connection.Thing.ThingName}/*",
    ]
  }

  # Things can read and write their own thing shadow.
  statement {
    sid = "ThingShadow"
    actions = [
      "iot:DeleteThingShadow",
      "iot:GetThingShadow",
      "iot:UpdateThingShadow",
    ]
    resources = [
      "arn:aws:iot:${var.region}:${local.aws_account_id}:thing/$${iot:Connection.Thing.ThingName}/*",
    ]
  }
}

resource "aws_iot_topic_rule" "cloudwatch_metrics_rule" {
  name        = "temperature"
  description = "Extract the crawl space's temperature."
  enabled     = true
  sql         = "SELECT state.reported.temperature FROM '$aws/things/${aws_iot_thing.sensor.name}/shadow/update'"
  sql_version = "2016-03-23"

  cloudwatch_metric {
    metric_name      = local.cloudwatch_temperature_metric
    metric_namespace = var.group_name
    metric_unit      = "None"
    metric_value     = "$${state.reported.temperature}"
    # metric_timestamp = "$${timestamp()}"
    role_arn = aws_iam_role.iot_cloudwatch_metrics_rule.arn
  }
}

resource "aws_iam_role" "iot_cloudwatch_metrics_rule" {
  name               = "${var.group_name}-iot-cloudwatch-metrics-rule"
  assume_role_policy = data.aws_iam_policy_document.iot_assume_role_policy.json

  tags = {
    application = var.group_name
  }
}

resource "aws_iam_role_policy" "iot_cloudwatch_metrics_rule_policy" {
  name   = "cloudwatch-policy"
  role   = aws_iam_role.iot_cloudwatch_metrics_rule.id
  policy = data.aws_iam_policy_document.iot_cloudwatch_metrics_rule_policy.json
}

data "aws_iam_policy_document" "iot_cloudwatch_metrics_rule_policy" {
  statement {
    actions = ["cloudwatch:PutMetricData"]
    resources = [
      "*",
    ]
  }
}
