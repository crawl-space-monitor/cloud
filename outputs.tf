output "iot_endpoint" {
  value = data.aws_iot_endpoint.device.endpoint_address
}

output "aws_region" {
  value = var.region
}

output "sensor_certificate_path" {
  value = var.sensor_certificate_path
}

output "sensor_private_key_path" {
  value = var.sensor_private_key_path
}

output "sensor_thing_name" {
  value = aws_iot_thing.sensor.name
}

output "sensor_thing_arn" {
  value = aws_iot_thing.sensor.arn
}

output "cognito_user_pool_domain" {
  value = "${aws_cognito_user_pool_domain.user_pool.domain}.auth.${local.region}.amazoncognito.com"
}

output "cognito_user_pool_id" {
  value = aws_cognito_user_pool.user_pool.id
}

output "cognito_user_pool_ios_app_client_id" {
  value = aws_cognito_user_pool_client.ios_app.id
}

output "cognito_identity_pool_id" {
  value = aws_cognito_identity_pool.id_pool.id
}

output "registrar_lambda_arn" {
  value = aws_lambda_function.registrar.arn
}

output "registrar_alias_arn" {
  value = aws_lambda_alias.registrar.arn
}

output "registrar_qualifier" {
  value = aws_lambda_alias.registrar.name
}

output "pinpoint_app_id" {
  value = aws_pinpoint_app.crawl_space_monitor.application_id
}

output "app_site_domain" {
  value = local.cloudfront_app_site_fqdn
}
