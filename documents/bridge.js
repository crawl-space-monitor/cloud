const { DynamoDBClient, ScanCommand } = require('@aws-sdk/client-dynamodb');
const { PinpointClient, SendMessagesCommand } = require("@aws-sdk/client-pinpoint");
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb');

exports.handler = async (event, context) => {
    console.log('Received event:', JSON.stringify(event, null, 2));
    const message = event.Records[0].Sns.Message;
    const parsed_message = JSON.parse(message);

    var sms_message = '';
    var apns_sound = "default";

    switch (parsed_message.NewStateValue) {
      case 'ALARM':
        sms_message = `[SRV] La température du vide sanitaire est EN DESSOUS DE ${parsed_message.Trigger.Threshold} ℃.`;
        apns_sound = {
          critical: 1,
          name: "temperature-alarm.caf",
          volume: 1.0,
        };
        break;

      case 'OK':
        sms_message = `[SRV] La température du vide sanitaire est au dessus de ${parsed_message.Trigger.Threshold} ℃.`;
        break;

      case 'INSUFFICIENT_DATA':
        sms_message = `[SRV] Le senseur du vide sanitaire ne produit plus de données.`;
        break;

      default:
        sms_message = `[SRV] Le senseur du vide sanitaire a produit une alerte inconnue (${parsed_message.NewStateValue}).`;
        break;
    }

    const apns_message = {
      aps: {
        alert: {
          title: "Alerte de froid",
          body: sms_message,
        },
        sound: apns_sound,
        "mutable-content": 1
      },
      MessageType: "TemperatureAlarm",
      NewStateValue: parsed_message.NewStateValue,
      Threshold: parsed_message.Trigger.Threshold,
    };

    const tokens = await readDeviceTokens();

    if (tokens.length > 0) {
      await sendNotifications(tokens, apns_message);
    }

    return {};
};

async function readDeviceTokens() {
  const client = new DynamoDBClient();
  const current_time = Math.floor(new Date().getTime() / 1000);
  const command = new ScanCommand({
    TableName: process.env.DYNAMODB_TABLE,
    IndexName: "Devices",
    FilterExpression: "#ttl > :ttl",
    ExpressionAttributeNames: {
        "#ttl": "TTL"
    },
    ExpressionAttributeValues: marshall({
        ":ttl": current_time
    })
  });

  try {
    const result = await client.send(command);
    
    return result.Items.map(item => {
      const record = unmarshall(item);
      return record.DeviceToken;
    });
  } catch (err) {
      console.error(`readDeviceTokens: DynamoDB.scan returned ${err}.`);
      throw err;
  }
}

async function sendNotifications(deviceTokens, apns_message) {
  const client = new PinpointClient();
  const addresses = {};

  deviceTokens.forEach(tok => {
    addresses[tok] = { ChannelType: "APNS_SANDBOX" };
  });

  const command = new SendMessagesCommand({
    ApplicationId: process.env.APPLICATION_ID,
    MessageRequest: {
      Addresses: addresses,
      MessageConfiguration: {
        APNSMessage: {
          RawContent: JSON.stringify(apns_message),
        },
      },
    },
  });

  try {
    const result = await client.send(command);

    console.info(`result = ${JSON.stringify(result)}`);

  } catch (err) {
      console.error(`sendNotifications: Pinpoint.sendMessages returned ${err}.`);
      throw err;
  }
}
