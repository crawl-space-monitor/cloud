const { DynamoDBClient, PutItemCommand } = require('@aws-sdk/client-dynamodb');
const { marshall } = require('@aws-sdk/util-dynamodb');

exports.handler = async (event, context) => {
  console.log('Received event:', JSON.stringify(event));

  // Get the current time in epoch second format
  const current_time = Math.floor(new Date().getTime() / 1000);
  // The record expires ~6 months from now
  const ttl = 60 * 60 * 24 * 31 * 6;

  event.TTL = current_time + ttl;

  try {
    await putDeviceRecord(event);

    console.log("Created or updated database record.")

    return {};
  } catch (err) {
    console.warn(`putDeviceRecord failed: ${err}`)
    throw err;
  }
};

async function putDeviceRecord(record) {
  const client = new DynamoDBClient();
  const command = new PutItemCommand({
    Item: marshall(record),
    TableName: process.env.DYNAMODB_TABLE,
  });
  
  try {
		await client.send(command);
  } catch (err) {
    console.error(`putDeviceRecord: DynamoDB.putItem returned ${err}.`);
    throw err;
  }
}
