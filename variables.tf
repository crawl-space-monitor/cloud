variable "region" {
  default = "ca-central-1"
}

variable "cloudfront_region" {
  default = "us-east-1"
}

variable "sms_region" {
  default = "us-east-1"
}

variable "sensor_private_key_path" {}
variable "sensor_certificate_path" {}
variable "apns_dev_certificate_path" {}
variable "apns_dev_private_key_path" {}
variable "apns_prod_certificate_path" {}
variable "apns_prod_private_key_path" {}
variable "apns_token_private_key_path" {}

variable "group_name" {
  default = "crawl-space-monitor"
}

locals {
  stack_tag = "crawl-space-monitor-cloud"
}

variable "temperature_alarm_threshold" {
  default = 1
}

variable "apns_bundle_id" {
  default = "ca.paullalonde.crawl-space-monitor"
}

variable "apns_team_id" {
  default = "5GKLRH676A"
}

variable "apns_token_key_id" {
  default = "D5ZHBAP34U"
}
